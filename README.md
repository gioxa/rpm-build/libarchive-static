## Libarchive static

creates a static rpm libarchive for odagrun

with configuration:

```bash
./configure --disable-rpath \
           --enable-silent-rules \
           --with-gnu-ld \
           --without-iconv \
           --without-lzo2 \
           --without-lz4 \
           --without-nettle \
           --without-openssl \
           --without-xml2 \
           --without-expat \
           --disable-acl \
           --disable-bsdtar \
           --disable-bsdcpio
```

and for fedora 29 srpm

with

changelog spec:

```
%changelog
* Sun Nov 25 2018 Danny Goossen <danny@gioxa.com> - 3.3.2-20s
- Rebuild for odagrun

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

```

### INSTALL

```
curl https://repo.deployctl.com/repo.rpm.sh | sudo bash
yum install libarchive-static
```

----

*deployed with [deployctl][https://www.deployctl.com]*
